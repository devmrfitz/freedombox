# SPDX-License-Identifier: AGPL-3.0-or-later
"""
Forms for msmtp app.
"""

from django import forms
from django.utils.translation import ugettext_lazy as _


class MSMTPForm(forms.Form):
    """Form to select a TLS domain for Quassel."""
    user = forms.CharField(
        label=_('User'),
        help_text=_("Enter SMTP username here"),
        required=True,
    )

    password = forms.CharField(
        max_length=32,
        required=True,
        help_text=_("Please enter your password"),
        widget=forms.PasswordInput)

    host = forms.CharField(
        label=_('host'),
        help_text=_("Enter host here"),
        required=False,
    )

    port = forms.CharField(
        label=_('TLS lol'),
        help_text=_("Enter SMTP port here"),
        required=False,
    )

    notify = forms.BooleanField(
        required=False, label="Check this to enable notification emails")

    test_notif = forms.BooleanField(
        required=False,
        label="Check this if you want a test notification to be sent")
