# SPDX-License-Identifier: AGPL-3.0-or-later
"""
URLs for the Msmtp module.
"""
from django.urls import path
from django.conf.urls import url

from .views import MSMTPAppView

urlpatterns = [
    url(r'^apps/msmtp/$', MSMTPAppView.as_view(), name="index"),
]
