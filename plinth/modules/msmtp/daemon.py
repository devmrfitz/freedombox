#!/usr/bin/python3
# SPDX-License-Identifier: AGPL-3.0-or-later
import os
from plinth import actions


def notify(notification):
    config_location = "/etc/msmtprc"
    config_exists = os.path.exists(config_location)
    configuration = eval(actions.superuser_run('msmtp',
                                                   ['get_config']))
    if config_exists and configuration['notify']:
        address = ''
        for line in open("/etc/msmtprc"):
            if line.startswith('from'):
                address = line.split()[1]
                break
        title = message = ''
        if notification.title:
            title = notification.title
        if notification.message:
            message = notification.message
        actions.run('msmtp', ['send', '--to', address,
                              '--body', "Notification message= " + message,
                              '--subject', "Notification: " + title])
