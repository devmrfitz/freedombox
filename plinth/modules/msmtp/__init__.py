from plinth import app as app_module
from plinth.modules.backups.components import BackupRestore
from . import manifest
from django.utils.translation import ugettext_lazy as _
from plinth import menu, frontpage

app = None
version = 1
managed_packages = ['msmtp']
is_essential = True

_description = [

    _("""This is a basic helper module for setting up notification emails.
Once the module is installed, open /plinth/apps/msmtp URL

This will setup a msmtp config file in /etc.

Now all the notifications will be sent to your email address.
Also, additional emails can be sent using:
actions.run('msmtp', ['send', '--to', 'receipent@example.com',
            '--body',body,'--subject', subject])
""")]
config_location = "/etc/msmtprc"

_SYSTEM_USER = 'msmtp'


class MsmtpApp(app_module.App):
    app_id = 'msmtp'

    def __init__(self):
        super().__init__()
        info = app_module.Info(app_id=self.app_id, version=version,
                               name="MSMTP",
                               is_essential=is_essential,
                               description=_description,
                               icon_filename='msmtp')
        self.add(info)
        backup_restore = BackupRestore('backup-restore-msmtp',
                                       **manifest.backup)
        self.add(backup_restore)
        menu_item = menu.Menu('menu-msmtp', 'MSMTP',
                              'MSMTP', 'msmtp',
                              'msmtp:index', parent_url_name='apps')
        self.add(menu_item)
        shortcut = frontpage.Shortcut(
            'shortcut-msmtp', 'msmtp',
            short_description=_("Basic MSMTP helper application"),
            icon=info.icon_filename,
            url='/plinth/apps/msmtp')
        self.add(shortcut)


def setup(helper, old_version=None):
    """Install and configure the module."""
    helper.install(managed_packages)
