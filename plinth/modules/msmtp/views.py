from django.contrib import messages

from plinth import actions, views

from .forms import MSMTPForm

from plinth.notification import Notification


class MSMTPAppView(views.AppView):
    """Serve configuration page."""
    form_class = MSMTPForm
    app_id = 'msmtp'

    def get_initial(self):
        """Get the current settings from Transmission server."""
        status = super().get_initial()
        configuration = eval(actions.superuser_run('msmtp',
                                                   ['get_config']))
        for key in configuration:
            status[key] = configuration[key]

        return status

    def form_valid(self, form):
        """Apply the changes submitted in the form."""
        new_status = form.cleaned_data
        new_configuration = {
            'user': new_status.get('user'),
            'password': new_status.get('password'),
            'host': new_status.get('host'),
            'notify': new_status.get('notify'),
            'port': new_status.get('port'),
        }

        actions.superuser_run('msmtp', ['set_config', '--config', str(new_configuration)])
        messages.success(self.request, 'Configuration updated')

        if new_status.get('test_notif'):
            Notification.update_or_create(id='msmtp', title='Title of test notification',
                                          message='Body of test notification')

        return super().form_valid(form)
